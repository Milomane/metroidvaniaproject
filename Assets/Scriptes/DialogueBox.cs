﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueBox : MonoBehaviour
{
    public GlobalClasses.Dialog[] dialog;
    public int i = 0;
    
    public TMP_Text textObject;

    public GameObject nextIndicatorObject;
    public GameObject endIndicatorObject;

    public bool sentenceEnd;
    public bool dialogEnd;

    public float baseTimeBetweenChar = 0.03f;
    public float baseTextSize = 30;

    private bool start;


    public void Start()
    {
        StartDialogue();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && sentenceEnd)
        {
            // Play son de passage de dialogue

            if (dialogEnd)
            {
                Destroy(gameObject);
            }
            sentenceEnd = false;
            nextIndicatorObject.SetActive(false);
            StartCoroutine(DialogueEvent());
        }
    }

    public void StartDialogue()
    {
        StartCoroutine(DialogueEvent());
        start = true;
    }
    
    IEnumerator DialogueEvent ()
    {
        // Draw the text char / char

        textObject.text = "";
        foreach (char letter in dialog[i].textBox.ToCharArray())
        {
            textObject.text += letter;
            if (dialog[i].useCustomCharSpeed)
            {
                yield return new WaitForSeconds(dialog[i].timeBetweenChar);
            }
            else
            {
                yield return new WaitForSeconds(baseTimeBetweenChar);
            }
        }

        // Increase I
        i++;
        sentenceEnd = true;

        // Test if this is the final sentence
        if (dialog.Length <= i)
        {
            endIndicatorObject.SetActive(true);
            dialogEnd = true;
        }
        else
        {
            nextIndicatorObject.SetActive(true);
        }
    }
}