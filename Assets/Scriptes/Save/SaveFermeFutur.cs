﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveFermeFutur : MonoBehaviour
{
    public Canvas canvas;
    
    //Détecte s'il existe une clé (la clé de la sauvegarde de la scène. Si elle existe, la sauvegarde est chargée (bout de scotch pour le canvas, sinon, celui-ci est bugué et zoomé, donc on le dézoome)
    public void Awake()
    {
        canvas = FindObjectOfType<Canvas>();
        var autoSaveMgr = GameObject.Find("Easy Save 3 Manager").GetComponent<ES3AutoSaveMgr>();

        if (ES3.KeyExists("SaveFermeFutur"))
        {
            autoSaveMgr.Load();
            canvas.planeDistance = 1;
        }
    }
}