﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNewArea : MonoBehaviour
{
    public string sceneToLoad;
    public string spawnPoint;

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        if (collision.tag == "Player")
        {
            Debug.Log("Warp");
            collision.GetComponent<PlayerController>().spawnPoint = spawnPoint;
            Application.LoadLevel(sceneToLoad);
        }
    }
}
