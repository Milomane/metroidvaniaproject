﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnpoint : MonoBehaviour
{
    private PlayerController thePlayer;
    private Camera theCamera;

    void Start()
    {
        // Find the player object
        thePlayer = FindObjectOfType<PlayerController>();
        theCamera = FindObjectOfType<Camera>();

        // Make sure to spawn on the right spawnpoint
        if (thePlayer.GetComponent<PlayerController>().spawnPoint == gameObject.name)
        {
            // Place the player on the position of the spawnpoint
            thePlayer.transform.position = transform.position;
            theCamera.transform.position = transform.position + new Vector3(0,0,-10); ;
        }
    }
}