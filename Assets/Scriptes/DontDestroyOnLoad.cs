﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private static bool objectExist;
    
    void Start()
    {
        if (!objectExist)
        {
            objectExist = true;
            DontDestroyOnLoad(transform.gameObject); // Keep the same object between scenes
        } else {
            Destroy(gameObject);
        }
    }
}
