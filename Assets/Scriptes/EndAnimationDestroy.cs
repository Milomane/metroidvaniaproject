﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndAnimationDestroy : MonoBehaviour
{
    Animator animator;
    public float delay;

    public void Start()
    {
        animator = GetComponent<Animator>();
        Destroy (gameObject, animator.GetCurrentAnimatorStateInfo(0).length + delay); 
    }
}
