﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    public GameObject menuPause;

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            
            if (menuPause.active)
            {
                menuPause.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                menuPause.SetActive(true);
                Time.timeScale = 0;
            }
                
        }
    }

    public void Reprendre()
    {
        menuPause.SetActive(false);
        Time.timeScale = 1;
    }

    public void Sauvegarde()
    {
        var autoSaveMgr = GameObject.Find("Easy Save 3 Manager").GetComponent<ES3AutoSaveMgr>();
        ES3.Save<string>("SavedScene", SceneManager.GetActiveScene().name);
        autoSaveMgr.Save();
    }

    public void Menu()
    {
        SceneManager.LoadScene("MenuStart");
    }

    public void Quitter()
    {
        Application.Quit();
    }
}
