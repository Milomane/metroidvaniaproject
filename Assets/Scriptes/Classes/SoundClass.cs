﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundClass
{
    public string name;
    
    public AudioClip audioClip;

    public bool loop;
    
    [Range(0,1)]
    public float volume;

    [Range(.1f, 3)] public float pitch;
    [HideInInspector]
    public AudioSource source;
}
