﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GlobalClasses
{
    [System.Serializable]
    public class Dialog
    {
        [TextArea]
        public string textBox;
        public bool useCustomCharSpeed;
        [ShowIf("useCustomCharSpeed")]
        public float timeBetweenChar;
        public bool useCustomTextSize;
        [ShowIf("useCustomTextSize")]
        public float textSize;
        public bool useCustomFont;
        [ShowIf("useCustomTextSize")]
        public FontData font;
    }
}