﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OuafCollider : MonoBehaviour
{
    public Transform pTransform;
    public AudioSource audio;
    public bool detectSheep, detectBoss;
    public GameObject moucon;
    public List<GameObject> sheeps = new List<GameObject>();

    public void Bark()
    {
        Vector3 inversePlayer = new Vector3();
        audio.Play();
        if (detectSheep)
        {
            foreach (var mouton in sheeps)
            {
                Debug.Log("oui");
                var force = transform.position - mouton.transform.position;
                force.Normalize();
                mouton.GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                mouton.GetComponentInParent<Rigidbody2D>().AddForce(-force * 8, ForceMode2D.Impulse);
                if (mouton.GetComponentInParent<Rigidbody2D>().velocity == new Vector2(0, 0))
                {
                    mouton.GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                }
                
            }
        }

        if (detectBoss)
        {
            var force = transform.position - moucon.transform.position;
            force.Normalize();
            moucon.GetComponentInParent<Rigidbody2D>().AddForce(-force * 8, ForceMode2D.Impulse);
        }
    }
    
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Mouton"))
        {
            detectSheep = true;
            sheeps.Add(other.gameObject);
        }
        
        
    }
    
    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Mouton"))
        {
            detectSheep = true;
        }
        
        if(other.gameObject.CompareTag("BossMouton"))
        {
            detectBoss = true;
            moucon = other.gameObject;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Mouton"))
        {
            Debug.Log("test");
            detectSheep = false;
            
            sheeps.Remove(other.gameObject);
            //other.attachedRigidbody.AddForce(transform.forward);
        }
        
        if(other.gameObject.CompareTag("BossMouton"))
        {
            detectBoss = false;
            moucon = null;
        }
    }

    public void Update()
    {
        if (GetComponentInParent<PlayerController>().input != Vector2.zero)
        {
            transform.localPosition = GetComponentInParent<PlayerController>().input + new Vector2(0, 1);
        }
    }
}
