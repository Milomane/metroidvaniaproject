﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using UnityEngine;

public class ShowMap : MonoBehaviour
{
    private bool _onMap=false;
    public CinemachineVirtualCamera _cameraCineMachine;

    private Vector2 _startPosPlayer;

    private float _stopResize;
    
    private bool _changeSizeCamera;
    private bool _allowReSize;
    private bool _allowPressTouch=true;
    
    // Update is called once per frame
    void Update()
    {
        if (_allowReSize)
        {
            //ces boucles permettent de créer un dézoom ou un zoom de la caméra de manière fluide.
            if (_changeSizeCamera)
            {
                _stopResize = 50;
                _cameraCineMachine.m_Lens.OrthographicSize= Mathf.Lerp(_cameraCineMachine.m_Lens.OrthographicSize,_stopResize,Time.deltaTime*1f);
            }
            
            else
            {
                _stopResize = 10;
                _cameraCineMachine.m_Lens.OrthographicSize= Mathf.Lerp(_cameraCineMachine.m_Lens.OrthographicSize,_stopResize,Time.deltaTime*1f);
            }

            if (_cameraCineMachine.m_Lens.OrthographicSize == _stopResize)
            {
                _allowReSize = false;
                _changeSizeCamera = !_changeSizeCamera;
            }            
        }
    //si le joueur effectue la commande pour afficher la map, alros le jeu regarde si le joueur est déjà sur la map ou pas, si non,
    //il va dézommer de manière a ce que l'on puisse voir une plus grande partie de la map
    // et agrandir le player de manière a voir ou il est. Le programme va aussi retirer les collisions du joueur pour ne pas qu'il se fasse toucher en grandissant.
    //il va aussi y avoir un freeze dans le temps pour que ni le joueur ni quelqu'autre objet ne puisse bouger.
    //si le joeur et déjà sur la carte, il va y avoir les mêmes actions mais a l'envers.
        if (Input.GetKeyDown(KeyCode.Q)&&_allowPressTouch)
        {
            Time.timeScale = 1f;
            if (!_onMap)
            {
                _startPosPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
                GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider2D>().enabled = false;
                Sequence mySequence = DOTween.Sequence();
                _allowReSize = true;
                _changeSizeCamera = true;
                _allowPressTouch = false;
                mySequence.Append(GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().DOScale(8, 2).OnComplete(()=> FreezeTime()));
                _onMap = !_onMap;
            }
                
            else
            {
                    
                GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider2D>().enabled = true;
                Sequence mySequence = DOTween.Sequence();
                _allowReSize = true;
                _changeSizeCamera = false;
                _allowPressTouch = false;
                mySequence.Append(GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().DOMove(_startPosPlayer,2));
                mySequence.Join(GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().DOScale(1, 2).OnComplete(()=>PressAgain()));
                _onMap = !_onMap;
            }
        }
    }
    //cette fonction freeze le temps une fois que le joueut est sur la map.
    void FreezeTime()
    {
        Time.timeScale = 0;
        _allowPressTouch = true;
    }
//cette fonction et là pour éviter que le joueur puisse appuyer plusieus fois sur la touche correspondante a la map et ainsi créer des problèmes dans le jeu.
    void PressAgain()
    {
        _allowPressTouch = true;
    }
}
