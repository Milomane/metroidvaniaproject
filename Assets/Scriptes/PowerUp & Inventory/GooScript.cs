﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooScript : MonoBehaviour
{
    public bool inGoo;

    public PlayerController playerController;
    public BoxCollider2D collider;
    
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }
    void Update()
    {
        if (inGoo)
        {
            playerController.inGoo = true;
        }
        else
        {
            playerController.inGoo = false;
        }
    }

    public void enterGoo()
    {
        if (!playerController.playerHasBoots)
        {
            collider.enabled = true;
            // Dialogue et impossible pour le perso de passer
        }
        else
        {
            collider.enabled = false;
            inGoo = true;
            // Particule temps qu'il n'est pas sortie
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        inGoo = false;
    }
}
