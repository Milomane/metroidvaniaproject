﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public Objects[] objects;
    public int numberOfCase;

    public int realCounter;
    public int counter;
    public float smoothCounter;

    public float smooth;
    public GameObject prefabBox;

    public float radius;

    public float rotationOffSet;

    public PlayerController playerController;
    public GameObject helpTime;
    
    [System.Serializable]
    public class Objects
    {
        public GameObject prefab;
        public bool active;
    }

    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        
        numberOfCase = objects.Length;
        
        float needAngle = 360 / numberOfCase;
        for (int i = 0; i < numberOfCase; i++)
        {
            Quaternion rotation = Quaternion.AngleAxis(i * needAngle, Vector3.forward);
            Vector3 direction = rotation * Vector3.right;
            Vector3 position = transform.position + (direction * radius);
            
            GameObject obj = Instantiate(prefabBox, transform);
            obj.transform.position = position;
            obj.GetComponent<caseScript>().objectInCase(objects[i].prefab);
        }
        
    }
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            int startCount = realCounter;
            
            counter += 1 * Mathf.RoundToInt(Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
            realCounter += 1 * Mathf.RoundToInt(Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
            
            if (realCounter >= numberOfCase)
            {
                realCounter = 0;
            } else if (realCounter <= -1)
            {
                realCounter = numberOfCase - 1;
            }
            
            while (!objects[realCounter].active)
            {
                if (realCounter == startCount) break;

                Debug.Log(objects[realCounter].active);
                if (Mathf.RoundToInt(Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"))) > 0)
                {
                    realCounter++;
                    counter++;
                }
                else
                {
                    realCounter--;
                    counter--;
                }
                
                if (realCounter >= numberOfCase)
                {
                    realCounter = 0;
                } else if (realCounter <= -1)
                {
                    realCounter = numberOfCase - 1;
                }
            }
        }

        smoothCounter = Mathf.Lerp(smoothCounter, counter, smooth);
        
        transform.rotation = Quaternion.Euler(0, 0, transform.rotation.z - smoothCounter * 360 / numberOfCase + rotationOffSet);
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            switch (realCounter)
            {
                // WateringCan
                case 2:
                    playerController.WateringUse();
                    break;
            }
        }

        playerController.lanternActive = realCounter == 1;
        playerController.dogActive = realCounter == 3;
        playerController.timeTravelActive = realCounter == 4;

        if (realCounter == 4)
        {
            helpTime.SetActive(true);
        }
        else
        {
            helpTime.SetActive(false);
        }
    }
}
