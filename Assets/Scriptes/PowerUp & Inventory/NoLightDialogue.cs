﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoLightDialogue : MonoBehaviour
{
    public NoLightScript noLightScript;
    public GameObject dialogueNoLight;
    
    void Start()
    {
        noLightScript = GetComponentInParent<NoLightScript>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            noLightScript.enterNoLight();
            
            if (!FindObjectOfType<DialogueBox>() && !FindObjectOfType<PlayerController>().lanternActive)
            {
                Transform dialogueTransform = GameObject.FindGameObjectWithTag("DialogueManager").transform;
                Instantiate(dialogueNoLight, dialogueTransform);
            }
        }
    }
}