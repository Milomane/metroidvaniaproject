﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GooDialogue : MonoBehaviour
{
    public GooScript gooScript;
    public GameObject dialogueGoo;
    
    void Start()
    {
        gooScript = GetComponentInParent<GooScript>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            GetComponentInParent<GooScript>().enterGoo();
            
            if (!FindObjectOfType<DialogueBox>() && !FindObjectOfType<PlayerController>().playerHasBoots)
            {
                Transform dialogueTransform = GameObject.FindGameObjectWithTag("DialogueManager").transform;
                Instantiate(dialogueGoo, dialogueTransform);
            }
        }
    }
}
