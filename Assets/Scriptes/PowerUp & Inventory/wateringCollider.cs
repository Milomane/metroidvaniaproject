﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wateringCollider : MonoBehaviour
{
    public bool use;
    public GameObject otherCollider;

    private float detectTimer;

    public void Update()
    {
        if (GetComponentInParent<PlayerController>().input != Vector2.zero)
        {
            transform.localPosition = GetComponentInParent<PlayerController>().input;
        }

        if (detectTimer > 0)
        {
            detectTimer -= Time.deltaTime;
            if (use)
            {
                otherCollider.GetComponent<PlantScript>().Active();
            }
        }
        else
        {
            use = false;
            otherCollider = null;
        }
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Plant")
        {
            detectTimer = 0.1f; 
            otherCollider = other.gameObject;
        }
    }
}
