﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoLightScript : MonoBehaviour
{
    public bool inNoLight;

    public PlayerController playerController;
    public BoxCollider2D collider;
    
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    public void enterNoLight()
    {
        if (!playerController.lanternActive)
        {
            collider.enabled = true;
            // Dialogue et impossible pour le perso de passer
        }
        else
        {
            collider.enabled = false;
            inNoLight = true;
            // Particule temps qu'il n'est pas sortie
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        inNoLight = false;
    }
}