﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeTravel : MonoBehaviour
{
    public RoomTimes roomTimes;
    public bool active;
    private PlayerController player;
    
    [System.Serializable]
    public class RoomTimes
    {
        public string past;
        public string present;
        public string futur;
    }

    public enum epoque
    {
        past,
        present,
        future
    }

    public void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    public void Update()
    {
        if (active)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                timeTravel(epoque.past);
            }
            if (Input.GetKeyDown(KeyCode.B))
            {
                timeTravel(epoque.present); 
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                timeTravel(epoque.future);
            }
        }
    }


    public void timeTravel(epoque epoqueChoose)
    {
        player.spawnPoint = null;
        
        string nextScene = SceneManager.GetActiveScene().name;
        
        switch (epoqueChoose)
        {
            case epoque.past:
            {
                nextScene = roomTimes.past;
                break;
            }
            case epoque.present:
            {
                nextScene = roomTimes.present;
                break;
            }
            case epoque.future:
            {
                nextScene = roomTimes.futur;
                break;
            }
        }

        if (SceneManager.GetActiveScene().name != nextScene)
        {
            SceneManager.LoadScene(nextScene);
        }
    }
}