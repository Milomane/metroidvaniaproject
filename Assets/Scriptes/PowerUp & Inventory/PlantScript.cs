﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantScript : MonoBehaviour
{
    public GameObject teleporter;
    
    public void Active()
    {
        GetComponent<Animator>().SetBool("Active", true);
        teleporter.SetActive(true);
    }
}
