﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YPositionOnLayer : MonoBehaviour
{
    public bool updatePos;
    void Start()
    {
        GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(-transform.position.y);
    }
    
    void Update()
    {
        if (updatePos)
        {
            GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt(-transform.position.y);
        }
    }
}
