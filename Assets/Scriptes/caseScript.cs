﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class caseScript : MonoBehaviour
{
    private Inventory inventory;
    
    void Start()
    {
        inventory = GetComponentInParent<Inventory>();
    }
    void Update()
    {
        transform.localRotation = Quaternion.Euler(0, 0, transform.rotation.z + (inventory.smoothCounter * 360 / inventory.numberOfCase - inventory.rotationOffSet));
    }

    public void objectInCase(GameObject objectToInstansiate)
    {
        GameObject obj = Instantiate(objectToInstansiate, transform);
        obj.transform.localPosition = Vector3.zero;
        obj.GetComponent<SpriteRenderer>().sortingLayerName = "UI";
        obj.GetComponent<SpriteRenderer>().sortingOrder = 10;
        Debug.Log(obj.GetComponent<SpriteRenderer>().sortingOrder);
    }
}
