﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public GameObject nextText;
    public string sceneNameToLoad;

    private void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(gameObject.GetComponent<TextMeshProUGUI>().DOFade(0,2).OnComplete(NextText));
    }

    void NextText()
    {
        nextText.transform.DOMoveY(7.5f, 25).OnComplete(NextScene);
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit") || Input.GetButtonDown("Cancel") || Input.GetButtonDown("Jump"))
        {
            NextScene();
        }
    }

    void NextScene()
    {
        Debug.Log("changement de scène");
        SceneManager.LoadScene(sceneNameToLoad);
    }
}
