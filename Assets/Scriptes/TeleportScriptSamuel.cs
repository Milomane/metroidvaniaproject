﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class TeleportScriptSamuel : MonoBehaviour
{
    public GameObject teleportLinkWith;

    public float decalageX;
    public float decalageY;
    public float tolerance;

    private void OnTriggerEnter2D(Collider2D other)
    {
        //compare la direction d'ou vient le player ce qui permet de le faire réapparaître dans la continuité de son mouvement
        //la tolérance permet de comparer la position "y" sinon le programme ne s'occupait que des "x"
        if (other.gameObject.CompareTag("Player"))
        {
            GameObject vCamera = FindObjectOfType<CinemachineVirtualCamera>().gameObject;
            vCamera.gameObject.SetActive(false);
            
            if (other.gameObject.transform.position.x < gameObject.transform.position.x && Math.Abs(other.gameObject.transform.position.y - gameObject.transform.position.y) < tolerance )
            {
                other.gameObject.transform.position = teleportLinkWith.transform.position + new Vector3(decalageX,0,0);
            }
            
            else if(other.gameObject.transform.position.x > gameObject.transform.position.x && Math.Abs(other.gameObject.transform.position.y - gameObject.transform.position.y) < tolerance)
            {
                other.gameObject.transform.position = teleportLinkWith.transform.position + new Vector3(-decalageX,0,0);
            }    
            
            else if(other.gameObject.transform.position.y > gameObject.transform.position.y)
            {
                other.gameObject.transform.position = teleportLinkWith.transform.position + new Vector3(0,-decalageY,0);
            }
            
            else if(other.gameObject.transform.position.y < gameObject.transform.position.y)
            {
                other.gameObject.transform.position = teleportLinkWith.transform.position + new Vector3(0,decalageY,0);
            }
            
            vCamera.gameObject.SetActive(true);
        }
    }
}
