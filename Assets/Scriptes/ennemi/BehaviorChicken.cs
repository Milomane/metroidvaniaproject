﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorChicken : MonoBehaviour
{
    private float _angle;
    public Transform pTransform;

    public bool allowFollow;
    private bool _contactPlayer;

    public GameObject poussin;

    public int damage;
    
    private void Start()
    {
        InvokeRepeating("SpawnPoussin",0,2);
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;

    }

    private void Update()
    {
        //récupère dans l'enfant qui est le circle collider l'information pour savoir si il peut passer en mode offensif
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
        }
    }
    //fait des dégâts au player
    public void DamagePlayer()
    {
        _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
        if (_contactPlayer)
        {
            Debug.Log("contact");
            //other.GetComponent<>().
            pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
        }

    }
//boucle qui permet de faire apparaître des poussins.
    void SpawnPoussin()
    {
        if (allowFollow)
        {
            Instantiate(poussin, transform.position,Quaternion.identity);
        }
    }
}






