﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BehaviourRabbit : MonoBehaviour
{
    private float _angle;
    public Transform pTransform;

    public bool allowFollow;
    private bool _contactWall;
    private bool _contactPlayer;

    public int damage;
    
    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    private void Update()
    {
        //récupère dans l'enfant qui est le circle collider l'information pour savoir si il peut passer en mode offensif
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        _contactWall = GetComponentInChildren<CircleColliderRabbit>().contactWall;
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.Translate(Vector2.up*Time.deltaTime);
        }
        //fait l'animation de saut
        if (_contactWall)
        {
            gameObject.transform.DOScale(new Vector3(0.5f,0.5f,0.5f) , 2f);
        }
        else
        {
            gameObject.transform.DOScale(new Vector3(0.3f,0.3f,0.3f) , 2f);
        }
    }
    
    //fait des dégâts au player

    public void DamagePlayer(){
        {
            _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
            if (_contactPlayer)
            {
                //other.GetComponent<>().
                pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            }
        }
    }
}
