﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourGoat : MonoBehaviour
{
    private float _angle;
    private bool _allowFollow;
    private bool _allowRotate;
    public Transform pTransform;
    public float speed;
    private bool _contactPlayer;
    public int damage;

    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    
    void Update()
    {//le script reçoit l'autorisation ou non de pouvoir passer en mode agressif.
        _allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        if (_allowFollow)
        {
            if (_allowRotate)
            {//permet de se tourner dans la direction du player.
                Vector3 relative = transform.InverseTransformPoint(pTransform.position);
                _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
                transform.Rotate(0,0, -_angle);
                _allowRotate = false;
            }//une fois dans la bonne direction, va charger tout droit.
            transform.Translate(Vector2.up*speed*Time.deltaTime);
        }
        else
        {
            //permet de tourner pour la prochaine fois que le player rentrera dans la zone d'aggro.
            _allowRotate = true;
        }
    }
//fait des dégâts au player
    public void DamagePlayer(){
        {
            _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
            if (_contactPlayer)
            {
                //other.GetComponent<>().
                pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            }
        }
    }
}

