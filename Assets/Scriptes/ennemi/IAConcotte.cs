﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAConcotte : MonoBehaviour
{
    private float _angle;
    public Transform pTransform, spawnPoule1, spawnPoule2, spawnPoule3;

    public bool allowFollow;
    private bool _contactPlayer;

    public GameObject poule;

    public int damage;
    
    private void Start()
    {
        InvokeRepeating("SpawnPoule",0,4);
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;

    }

    private void Update()
    {
        //récupère dans l'enfant qui est le circle collider l'information pour savoir si il peut passer en mode offensif
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
        }
    }
    //fait des dégâts au player
    public void DamagePlayer()
    {
        _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
        if (_contactPlayer)
        {
            Debug.Log("contact");
            pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
        }

    }
//boucle qui permet de faire apparaître des poussins.
    void SpawnPoule()
    {
        if (allowFollow)
        {
            Instantiate(poule, spawnPoule1.transform.position,Quaternion.identity);
            Instantiate(poule, spawnPoule2.transform.position,Quaternion.identity);
            Instantiate(poule, spawnPoule3.transform.position,Quaternion.identity);
        }
    }
}
