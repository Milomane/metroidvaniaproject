﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TacheDeBoue : MonoBehaviour
{
    private float _countdownSetFalse = 4;

    private float _countdownDoScale=1;
    
    // Start is called before the first frame update
    void Start()
    {
        //fais apparaître l'objet de manière fluide
        gameObject.transform.DOScale(new Vector2(15, 15), 2);
    }

    // Update is called once per frame
    void Update()
    {
        //fais disparaître l'objet de manière fluide.
        _countdownSetFalse -= Time.deltaTime;
        if (_countdownSetFalse <= 0)
        {
            gameObject.transform.DOScale(new Vector2(0, 0), 3).OnComplete(()=>Destroy(gameObject));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerHealth>().Stun(3, .4f);
        }
    }
}
