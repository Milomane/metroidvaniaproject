﻿using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Environments;
using DG.Tweening;
using UnityEngine;

public class Cochonnard : MonoBehaviour
{   
    private bool _startDash;
    private bool _allowRotate=true;
    private bool _contactPlayer;
    
    private float _countdownDash = 10;
    private float _countdownStopDash = 2;
    private float _countdownMud=3;
    private float _angle;
    public float speed;

    private Transform _transformPlayer;

    public GameObject tacheBoue;
    public GameObject annonceTache;
    
    private int _nbDash;
    public int damage;

    // Start is called before the first frame update
    void Start()
    {
        _transformPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {       
        DamagePlayer();
        _startDash = GetComponentInChildren<DetectCircle>().detectPlayer;
//quand le boss n'est pas en phase de dash, il va lancer de la boue sur le joueur qui va la ralentir.
        if (_countdownDash >= 0)
        {
            _countdownMud -= Time.deltaTime;
            if (_countdownMud <= 0)
            {
                Instantiate(annonceTache, _transformPlayer.position, Quaternion.identity);
                _countdownMud = 3;
            }
        }
        //le boss va suivre le joueur du regard
        if (_allowRotate)
        {
            Vector3 relative = transform.InverseTransformPoint(_transformPlayer.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            _countdownDash -= Time.deltaTime;
            if (_countdownDash <= 0)
            {
                _allowRotate = false;
            }
        }
        //une fois que le boss peut dasher, il va le faire trois fois de suite puis il repartira dans la phase ou il lance de la boue.
        else
        {
            transform.Translate(Vector2.up*speed*Time.deltaTime);
            _countdownStopDash -= Time.deltaTime;
            if (_countdownStopDash <= 0)
            {
                _nbDash++;
                if (_nbDash >= 3)
                {
                    _countdownDash = 10;
                    _countdownStopDash = 2;
                    _allowRotate = true;
                    _nbDash = 0;
                }
                else
                {
                    _countdownStopDash = 2;
                    _allowRotate = true;
                }
            }
        }
    }
    public void DamagePlayer(){
        {
            _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
            if (_contactPlayer)
            {
                //other.GetComponent<>().
                _transformPlayer.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            }
        }
    }
}
