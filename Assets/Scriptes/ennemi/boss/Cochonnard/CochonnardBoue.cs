﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CochonnardBoue : MonoBehaviour
{    private float _countdownSetFalse = 4;

    public GameObject tacheBoue;
    // Start is called before the first frame update
    void Start()
    {//fais un scale pour faire apparaitre l'objet de manière fluide
        gameObject.transform.DOScale(new Vector2(2, 2), 3).OnComplete(()=>Instantiate(tacheBoue, transform.position,Quaternion.identity));
    }

    private void Update()
    {
        //fais disparaitre l'objet de manière fluide
        _countdownSetFalse -= Time.deltaTime;
        if (_countdownSetFalse <= 0)
        {
            
            Destroy(gameObject);
        }
    }
}
