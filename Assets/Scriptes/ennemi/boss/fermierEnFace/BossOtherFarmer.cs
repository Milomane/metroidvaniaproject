﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Serialization;

public class BossOtherFarmer : MonoBehaviour
{
    public Transform[] routes;
    private Transform _playerTransform;
    
    private int _routeToGo;

    private float _tParamSave;
    private float _tParam;
    public float speedModifier;
    private float _angle;
    private float _angleSave;
    
    private Vector2 _catPosition;
    private Vector2 _saveCatPosition;
    
    private bool _coroutineAllowed;
    private bool _playerDetected;

    public GameObject circleColliderPlayer;

    private void Start()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _routeToGo = 0;
        _tParam = 0;
        _coroutineAllowed = true;
    }

    private void Update()
    {
        
        if (_coroutineAllowed)
        {
            StartCoroutine(GoByTheRoute(_routeToGo));
        }

        if (_playerDetected)
        {
            OnPlayerDetect();
        }

        if (PlayerHide.playerCache)
        {
            circleColliderPlayer.SetActive(false);
        }
        else
        {
            circleColliderPlayer.SetActive(true);
            _playerDetected = GetComponentInChildren<DetectCircle>().detectPlayer;
        }

    }

    private IEnumerator GoByTheRoute(int routeNumber)
    {
        _coroutineAllowed = false;

        Vector2 p0 = routes[routeNumber].GetChild(0).position;
        Vector2 p1 = routes[routeNumber].GetChild(1).position;
        Vector2 p2 = routes[routeNumber].GetChild(2).position;
        Vector2 p3 = routes[routeNumber].GetChild(3).position;
        
//cette boucle va permettre de dessiner la courbe point par point en utilisant la formule de la courbe de bézier.
//la formule mettra chaque point dans un vecteur qui fera que la position de l'objet suivra ces points.
//elle permettra aussi de faire regarder l'objet dans la direction dans laquelle il va.

        while (_tParam<1)
        {   
            _catPosition = _saveCatPosition;
            _tParam += Time.deltaTime * speedModifier;
            
            _catPosition = Mathf.Pow(1 - _tParam, 3) * p0 +
                              3 * Mathf.Pow(1 - _tParam, 2) * _tParam * p1 +
                              3 * (1 - _tParam) * Mathf.Pow(_tParam, 2) * p2 +
                              Mathf.Pow(_tParam, 3) * p3;
            
            _tParamSave = _tParam;
            _saveCatPosition = _catPosition;
            
            if (_playerDetected)
            {
                
                _tParamSave = _tParam;
                _saveCatPosition = _catPosition;
                break;
            }
            Vector3 relative = transform.InverseTransformPoint(_catPosition);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.position = _catPosition;
  
            yield return new WaitForEndOfFrame();
            
        }

        
        if (!_playerDetected)
        {
            _tParam = 0;
            _routeToGo ++;
        }
        
        if (_routeToGo > routes.Length - 1)
        {
            _routeToGo = 0;
        }

        if (!_playerDetected)
        {
            _coroutineAllowed = true;
        }
    }

    void OnPlayerDetect()
    {
        Vector3 relative = transform.InverseTransformPoint(_playerTransform.position);
        _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
        transform.Rotate(0,0, -_angle);
        transform.Translate(Vector3.up*speedModifier*Time.deltaTime);
    }

    void GoBack()
    {
        _coroutineAllowed = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("fin détection");
            transform.DOMove(_saveCatPosition, 2).OnComplete(GoBack);
        }

    }
}
