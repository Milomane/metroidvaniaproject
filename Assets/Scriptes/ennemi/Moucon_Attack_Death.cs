﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moucon_Attack_Death : MonoBehaviour
{
    public bool inContactWithPlayer;
    public bool isDefeated;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inContactWithPlayer = true;
        }

        if (other.CompareTag("ZoneMoucon"))
        {
            isDefeated = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inContactWithPlayer = false;
        }
    }
}
