﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LittleChickenBehaviour : MonoBehaviour
{
    private float _angle;
    private Transform _playerTransform;
    public float speed;

    public int damage;
    // Start is called before the first frame update
    void Start()
    {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 relative = transform.InverseTransformPoint(_playerTransform.position);
        _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
        transform.Rotate(0,0, -_angle);
        transform.Translate(Vector3.up*speed*Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
