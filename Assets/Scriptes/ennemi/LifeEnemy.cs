﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeEnemy : MonoBehaviour
{
    public int maxLifePoint, currentLife;
    public bool isDead = false;

    public float blinckRate = 0.2f;
    public float invinssibilityTime = 0.5f;
    private float timer;
    

    void Awake()
    {
        currentLife = maxLifePoint;
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        
        if (currentLife <= 0 && !isDead)
        {
            isDead = true;
            Death();
        }
    }

    public void Hurt(int damage, Vector3 damageOriginPoint)
    {
        currentLife -= damage;
        StartCoroutine("Blinck");
    }

    public void Death()
    {
        Destroy(gameObject);
    }
    
    public IEnumerator Blinck()
    {
        timer = invinssibilityTime;
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        
        while (timer > 0)
        {
            renderer.color = Color.red;
            yield return new WaitForSeconds(blinckRate);
            renderer.color = Color.white;
            yield return new WaitForSeconds(blinckRate);
        }
    }
}
