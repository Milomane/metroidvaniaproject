﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PriseAggro_Mouton : MonoBehaviour
{
    public bool inContactWithPlayer = false;


    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inContactWithPlayer = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            inContactWithPlayer = false;
        }
    }
}
