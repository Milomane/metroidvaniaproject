﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviourPig : MonoBehaviour
{
    private bool _launchMud;
    private bool _startDash;
    private bool _allowRotate;
    private bool _allowDash;
    private bool _contactPlayer;
    
    public float speed;
    private float _angle;
    private float _countdownDash = 2;
    private float _countdownStopDash = 2;

    public Transform pTransform;
    private Transform _transformTarget;

    public GameObject boue;

    public int damage;
    private void Start()
    {
        InvokeRepeating(nameof(LaunchMud),0,1.5f);
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        _launchMud = GetComponentInChildren<DetectBouePig>().allowBoue;
        _startDash = GetComponentInChildren<DetectCircle>().detectPlayer;
        if (_startDash)
        {
            _launchMud = false;
            PrepDash();
        }
        else
        {
            //permet de tourner pour la prochaine fois que le player rentrera dans la zone d'aggro.
            _allowRotate = true;
            
        }

        if (_allowDash)
        {
            //une fois dans la bonne direction, va charger tout droit.
            _countdownDash -= Time.deltaTime;
            if (_countdownDash <= 0)
            {
                transform.Translate(Vector2.up*speed*Time.deltaTime);
                _countdownStopDash -= Time.deltaTime;
            }

            if (_countdownStopDash<=0)
            {
                _countdownDash = 2;
                _countdownStopDash = 2;
                _allowDash = false;
                _startDash = false;
            }
        }
    }

    private void PrepDash()
    {
            if (_allowRotate)
            {//permet de se tourner dans la direction du player.
                Vector3 relative = transform.InverseTransformPoint(pTransform.position);
                _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
                transform.Rotate(0,0, -_angle);
                _allowRotate = false;
            }
            _allowDash = true;
    }

    private void LaunchMud()
    {
        if (_launchMud && !_allowDash)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            //envoie de la boue sur le joueur
            _startDash = false;
            Instantiate(boue, transform.position, Quaternion.identity);
        }
    }
    //fait des dégâts au player
    public void DamagePlayer(){
        {
            _contactPlayer = GetComponentInChildren<ColliderContact>().onContactPlayer;
            if (_contactPlayer)
            {
                //other.GetComponent<>().
                pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            }
        }
    }
}
