﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mvmtBoue : MonoBehaviour
{
    public Transform pTransform;
    private float _angle;
    public int damage;
    private void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;

        Vector3 relative = transform.InverseTransformPoint(pTransform.position);
        _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
        transform.Rotate(0,0, -_angle);
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, 10);
        transform.Translate(Vector3.up*3*Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}