﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class IAMouton : MonoBehaviour
{
    private float _angle;
    public Transform pTransform; //Transform du player

    public bool allowFollow;
    public bool allowHitting;

    public bool hasHit = false;
    public bool hasBeenHit = false;
    public int damage;
    public float cooldown, cooldownBase;

    public LifeEnemy lifeEnemy;

    void Awake()
    {
        cooldown = cooldownBase; //On initie le cooldown 
        lifeEnemy = GetComponent<LifeEnemy>();
    }
    
    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Update()
    {
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        allowHitting = GetComponentInChildren<PriseAggro_Mouton>().inContactWithPlayer;
        
        if (lifeEnemy.currentLife < lifeEnemy.maxLifePoint)
        {
            hasBeenHit = true;
        }
        
        
        //permet de faire un lookAT (seulement s'il a été tapé avant) mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow && hasBeenHit)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.Translate(Vector2.up*Time.deltaTime);
        }

        if (allowHitting && hasBeenHit && !hasHit)
        {
            Coup(damage);
            hasHit = true;
        }
        
        //Si le mouton a tapé le player, on fait s'écouler un cooldown avant qu'elle puisse réattaquer
        if (hasHit)
        {
            cooldown -= Time.deltaTime;
            
            if (cooldown <= 0)
            {
                hasHit = false;
                cooldown = cooldownBase;
            }
        }
    }
    
    public void Coup(int degat)
    {
        Debug.Log("dégât");
        //PlayerHealth.FindObjectOfType<PlayerHealth>().TakeDamage(damage);
        pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
    }

    
}