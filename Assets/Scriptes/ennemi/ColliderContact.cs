﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderContact : MonoBehaviour
{
    public bool onContactPlayer;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            onContactPlayer = true;
        }
    }    
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            onContactPlayer = false;
        }
    }

    public void Update()
    {
        if (onContactPlayer)
        {
            if (GetComponentInParent<BehaviourGoat>())
            {
                GetComponentInParent<BehaviourGoat>().DamagePlayer();
            }
        }
    }
}
