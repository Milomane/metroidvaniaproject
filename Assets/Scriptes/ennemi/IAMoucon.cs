﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAMoucon : MonoBehaviour
{
    private float _angle;
    public Transform pTransform; //Transform du player

    public bool allowFollow;
    public bool allowHitting;
    public bool defeat;
    
    public bool isHitting = false;
    public int damage;
    public float cooldown, cooldownBase;


    void Awake()
    {
        cooldown = cooldownBase; //On initie le cooldown 
    }
    
    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    void Update()
    {
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        allowHitting = GetComponentInChildren<Moucon_Attack_Death>().inContactWithPlayer;
        defeat = GetComponentInChildren<Moucon_Attack_Death>().isDefeated;
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.Translate(Vector2.up*Time.deltaTime);
        }

        if (allowHitting && !isHitting)
        {
            Coup(damage);
            isHitting = true;
        }
        
        //Si Moucon a tapé le player, on fait s'écouler un cooldown avant qu'elle puisse réattaquer
        if (isHitting)
        {
            cooldown -= Time.deltaTime;
            
            if (cooldown <= 0)
            {
                isHitting = false;
                cooldown = cooldownBase;
            }
        }

        if (defeat)
        {
            Mort();
        }
    }
    
    public void Coup(int degat)
    {
        pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
    }

    public void Mort()
    {
        Debug.Log("Dead");
        Destroy(this);
    }
}
