﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAOie : MonoBehaviour
{
    private float _angle;
    public Transform pTransform; //Transform du player

    public bool allowFollow, allowHitting;
    
    public bool hasHit = false;
    public int damage, numberHit;
    public float cooldown, cooldownBase;

    void Awake()
    {
        cooldown = cooldownBase; //On initie le cooldown 
        numberHit = 0; //On initie le nombre de coups
    }
    
    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    void Update()
    {
        allowFollow = GetComponentInChildren<DetectCircle>().detectPlayer;
        allowHitting = GetComponentInChildren<EnemyHit>().inContactWithPlayer;
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.Translate(Vector2.up*Time.deltaTime);
        }

        if (allowHitting && !hasHit)
        {
            StartCoroutine(Coup(damage));
            //InvokeRepeating(Coup(damage), 0f, 0.2f);
            allowHitting = false;
        }
        
        //Si l'oie a tapé le player, on fait s'écouler un cooldown avant qu'elle puisse réattaquer
        if (hasHit)
        {
            cooldown -= Time.deltaTime;
            
            if (cooldown <= 0)
            {
                hasHit = false;
                cooldown = cooldownBase;

                allowHitting = true;
                numberHit = 0;
            }
        }
    }

    public IEnumerator Coup(int degat)
    {
        while (numberHit < 3)
        {
            Debug.Log("dégât");
            
            pTransform.gameObject.GetComponent<PlayerHealth>().TakeDamage(damage);
            
            numberHit++;
            yield return new WaitForSeconds(0.5f);
        }

        if (numberHit >= 3)
        {
            hasHit = true;
        }
    }
}
