﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunAne : MonoBehaviour
{
    public bool detectPlayer, canStun;

//quand le player rentre dans la zone d'aggro de l'ennemi, le script envoie une information au parent pour lui dire qu'il doit passer en mode attaque.
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            detectPlayer = true;
            canStun = true;
        }
    }
//quand le player sort de la zone d'aggro dit au parent de repasser en mode passif
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            detectPlayer = false;
            canStun = false;
        }
    }

}