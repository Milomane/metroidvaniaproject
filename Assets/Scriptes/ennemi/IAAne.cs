﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAAne : MonoBehaviour
{
    private float _angle;
    public Transform pTransform; //Transform du player

    public bool allowFollow, allowStun;
    
    public bool hasStun = false;
    public float cooldown, cooldownBase;

    public float stunTime;
    public float stunSpeedModifier;

    void Awake()
    {
        cooldown = cooldownBase; //On initie le cooldown 
    }
    
    public void Start()
    {
        pTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    void Update()
    {
        allowFollow = GetComponentInChildren<StunAne>().detectPlayer;
        allowStun = GetComponentInChildren<StunAne>().canStun;
        //permet de faire un lookAT mais seulement en 2D c'est a dire qu'il n'y a pas de rotation autour de l'axe y
        if (allowFollow)
        {
            Vector3 relative = transform.InverseTransformPoint(pTransform.position);
            _angle = Mathf.Atan2(relative.x, relative.y)*Mathf.Rad2Deg;
            transform.Rotate(0,0, -_angle);
            transform.Translate(-Vector2.up*Time.deltaTime);
        }

        if (allowStun && !hasStun)
        {
            Stun();
            hasStun = true;
        }
        
        //Si l'âne a étourdit le player, on fait s'écouler un cooldown avant qu'elle puisse réattaquer
        if (hasStun)
        {
            cooldown -= Time.deltaTime;
            
            if (cooldown <= 0)
            {
                hasStun = false;
                cooldown = cooldownBase;
            }
        }
    }

    public void Stun()
    {
        Debug.Log("Stun");
        /*PlayerController playerController = FindObjectOfType<PlayerController>();
        playerController.Stun();
        isHitting = true;*/
        pTransform.gameObject.GetComponent<PlayerHealth>().Stun(stunTime, stunSpeedModifier);
    }
}
