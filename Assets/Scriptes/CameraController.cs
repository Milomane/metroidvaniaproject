﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camera;
    public CinemachineVirtualCamera cinemachine;
    
    public GameObject targetTest;
    
    void Start()
    {
        cinemachine.Follow = FindObjectOfType<PlayerController>().transform;
        camera = Camera.main.gameObject;
    }

    public void SwitchCamera(GameObject newCamera)
    {
        camera.SetActive(false);
        camera.GetComponent<AudioListener>().enabled = false;
        
        newCamera.SetActive(true);
        newCamera.GetComponent<AudioListener>().enabled = true;
    }

    public void SwitchFollow(GameObject newTarget)
    {
        targetTest = cinemachine.Follow.gameObject;
        cinemachine.Follow = newTarget.transform;
    }
}
