﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    private static bool gameplayManagerExist;
    
    void Start()
    {
        if (!gameplayManagerExist)
        {
            gameplayManagerExist = true;
            DontDestroyOnLoad(transform.gameObject); // Keep the same object between scenes
        } else {
            Destroy(gameObject);
        }
    }
}