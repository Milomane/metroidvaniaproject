﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]

public class PlayerHealth : MonoBehaviour
{
    public float health;
    public int maxHealth = 5;

    public Color blinckColor;
    public float blinckRate;

    public float invinssibilityTime;
    private float timer = 0;
    private float _countdownHealth=1f;
    private int _damagePlayer;


    public Color colorHeal;
    public float healFlashTime;
    public ParticleSystem particleSystem;

    private Animator animator;
    private PlayerController playerController;

    private bool isColorChanging;
    private bool _damage=false;
    private bool _heal = false;

    
    public HealthBar healthBar;
    void Start()
    {
        healthBar.SetMaxHealth(maxHealth);

        health = maxHealth;
        animator = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
        particleSystem.Stop();
    }

    private void Update()
    {
        if (_damage)
        {
            _countdownHealth -= Time.deltaTime;
            if (_countdownHealth >= 0)
            {
                health = Mathf.Lerp(health,health-_damagePlayer,Time.deltaTime*0.1f);
                healthBar.SetHealth(health);
            }
            else
            {
                _countdownHealth = 1;
                _damage = false;
            }
        }
        else if (_heal)
        {
            _countdownHealth -= Time.deltaTime;
            if (_countdownHealth >= 0)
            {
                health = Mathf.Lerp(health,health+1,Time.deltaTime*0.1f);
                healthBar.SetHealth(health);
            }
            else
            {
                _countdownHealth = 1;
                _heal = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            TakeDamage(1);
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            Heal(1);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Stun(3f, 0.2f);
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        
        if (!isColorChanging)
        {
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var r in renderers)
            {
                r.color = Color.white;
            }
        }
    }

    public void Heal(int regen)
    {
        FindObjectOfType<audioManager>().Play("Heal");
        _heal = true;
        health += regen;
        if (health >= maxHealth)
        {
            health = maxHealth;
        }

        StartCoroutine("Flash");
    }
    
    public void TakeDamage(int damage)
    {
        FindObjectOfType<audioManager>().Play("Damage");
        _damagePlayer = damage;
        _damage = true;
        if (timer <= 0)
        {
            health -= damage;
            StartCoroutine("Blinck");
            animator.SetTrigger("Hurt");
        }
    }

    public void Stun(float stunTime, float speedMultiplier)
    {
        object[] parms = new object[2] {stunTime, speedMultiplier};
        playerController.StartCoroutine("Stun", parms);
    }

    public IEnumerator Blinck()
    {
        isColorChanging = true;
        
        timer = invinssibilityTime;
        
        while (timer > 0)
        {
            changeColor(blinckColor);
            yield return new WaitForSeconds(blinckRate);
            changeColor(Color.white);
            yield return new WaitForSeconds(blinckRate);
        }
        isColorChanging = false;
    }

    public IEnumerator Flash()
    {
        isColorChanging = true;
        
        changeColor(colorHeal);
        particleSystem.Play();
        yield return new WaitForSeconds(healFlashTime);
        changeColor(Color.white);
        particleSystem.Stop();

        isColorChanging = false;
    }

    public void changeColor(Color color)
    {
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (var r in renderers)
        {
            r.color = color;
        }
    }
}
