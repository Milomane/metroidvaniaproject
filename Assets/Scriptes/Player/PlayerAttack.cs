﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Animator animator;
    public float attackCooldown;
    public float cd;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (cd > 0)
        {
            cd -= Time.deltaTime;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                FindObjectOfType<audioManager>().Play("Attack");
                cd = attackCooldown;
                Attack();
            }
        }
    }
    
    public void Attack()
    {
        animator.SetTrigger("Attack");
    }
}
