﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.name + " " + other.tag);
        
        if (other.tag == "Enemy")
        {
            Debug.Log("Touch");
            GetComponentInParent<PlayerController>().DealDamage(other.gameObject);
        }
    }
}
