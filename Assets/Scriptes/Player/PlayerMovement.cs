﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float maxSpeed;
    public Vector2 moveSpeed;
    public float smoothLevel;


    public Animator animator;
    public Rigidbody2D rb;
    public PlayerController playerController;

    private Vector2 movement;
    private float tempMaxSpeed;

    private float _countdownSound = .25f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    
    // Update is called once per frame
    void Update()
    {
        movement = playerController.input;

        // Set animator variables
        if (movement.sqrMagnitude > 0.0001)
        {
            _countdownSound -= Time.deltaTime;
            if (_countdownSound<=0)
            {
                FindObjectOfType<audioManager>().Play("Walk");
                _countdownSound = .25f;
            }
            animator.SetFloat("HorizontalAxis", movement.x);
            animator.SetFloat("VerticalAxis", movement.y);
        }
        animator.SetFloat("Speed", movement.sqrMagnitude); // Magnitude take the length of the vector and sqr is a performance trick
    }

    void FixedUpdate()
    {
        //transform.position = (new Vector3(Mathf.Round(transform.position.x * 16) / 16, Mathf.Round(transform.position.y * 16) / 16));
        // Movement
        if (movement.x == 0)
        {
            moveSpeed.x = 0;
        }
        if (movement.y == 0)
        {
            moveSpeed.y = 0;
        }
        if (movement.y != 0 && movement.x != 0)
        {
            tempMaxSpeed = maxSpeed * Mathf.Sqrt(2)/2;
        } else
        {
            tempMaxSpeed = maxSpeed;
        }
        moveSpeed = new Vector2(Mathf.Lerp(moveSpeed.x, movement.x * tempMaxSpeed, smoothLevel), Mathf.Lerp(moveSpeed.y, movement.y * tempMaxSpeed, smoothLevel));

        rb.velocity = moveSpeed;
    }
}