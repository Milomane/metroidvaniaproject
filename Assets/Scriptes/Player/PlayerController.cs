﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;


[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PlayerHealth))]

public class PlayerController: MonoBehaviour
{
    public string spawnPoint;
    
    public Vector2 input;
    
    private static bool playerExist;

    private float health;
    public GameObject prefabDeathAnimation;
    
    public GameObject starStun;

    private PlayerMovement playerMovement;
    private PlayerHealth playerHealth;
    private Rigidbody2D rb;
    private Animator animator;

    public bool playerHasBoots;
    public bool inGoo;

    public bool lanternActive;
    public GameObject lantern;
    public Material shadowMaterial;
    public Material defaultMaterial;

    public bool dogActive;
    public GameObject dog;

    public bool timeTravelActive;
    
    public GameObject colliderWater;

    public int damage;
    
    //public static PlayerController playerController;

    void Start()
    {
        Debug.Log(health);
        animator = GetComponent<Animator>();
        playerMovement = GetComponent<PlayerMovement>();
        playerHealth = GetComponent<PlayerHealth>();
        rb = GetComponent<Rigidbody2D>();
        
        // Prevent for duplication of player when scene is load :
        if (!playerExist)
        {
            playerExist = true;
            DontDestroyOnLoad(transform.gameObject); // Keep the same object between scenes
        } else {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        light();
        Dog();
        TimeTravelAct();
        
        health = playerHealth.health;
        if (health <= 0)
        {
            PlayerDeath();
        }

        // Input
        input.x = Input.GetAxisRaw("Horizontal"); // Get value between 1 and -1
        input.y = Input.GetAxisRaw("Vertical"); // Get value between 1 and -1
    }

    

    public void PlayerDeath()
    {
        FindObjectOfType<audioManager>().Play("Death");
        Instantiate(prefabDeathAnimation, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public IEnumerator Stun(object[] parms)
    {
        float stunTime = (float)parms[0];
        float speedMultiplier = (float)parms[1];
        
        float initSpeed = playerMovement.maxSpeed;
        playerMovement.maxSpeed *= speedMultiplier;
        starStun.SetActive(true);
        yield return new WaitForSeconds(stunTime);
        playerMovement.maxSpeed = initSpeed;
        starStun.SetActive(false);
    }

    public void DealDamage(GameObject target)
    {
        target.GetComponent<LifeEnemy>().Hurt(damage, transform.position);
    }

    public void light()
    {
        lantern.SetActive(lanternActive);
        
        if (GameObject.FindObjectOfType<LevelManager>().needLight)
        {
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var r in renderers)
            {
                r.material = shadowMaterial;
            }
        }
        else
        {
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            foreach (var r in renderers)
            {
                r.material = defaultMaterial;
            }
        }
    }

    public void WateringUse()
    {
        colliderWater.GetComponent<wateringCollider>().use = true;
    }

    public void Dog()
    {
        if (dogActive)
        {
            dog.SetActive(dogActive);
            if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponentInChildren<OuafCollider>().Bark();
            }
        }
        else
        {
            dog.SetActive(false);
        }
    }

    public void TimeTravelAct()
    {
        if (timeTravelActive)
        {
            FindObjectOfType<LevelManager>().GetComponent<TimeTravel>().active = true;
        }
        else
        {

            FindObjectOfType<LevelManager>().GetComponent<TimeTravel>().active = false;
        }
    }
}