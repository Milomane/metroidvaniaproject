﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerYPositionOnLayer : MonoBehaviour
{
    public bool updatePos;
    
    void Start()
    {
        SpriteRenderer[] rendererArray = GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer renderer in rendererArray)
        {
            renderer.sortingOrder = Mathf.RoundToInt(-transform.position.y);
        }
    }
    
    void Update()
    {
        if (updatePos)
        {
            SpriteRenderer[] rendererArray = GetComponentsInChildren<SpriteRenderer>();

            foreach (SpriteRenderer renderer in rendererArray)
            {
                renderer.sortingLayerName = "Player";
                renderer.sortingOrder = Mathf.RoundToInt(-transform.position.y);
            }
        }
    }
}
