﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_EasySave : MonoBehaviour
{
    private int testInt;
    // Start is called before the first frame update
    void Start()
    {
        ES3.Save<int>("myIntSaved", 50);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            testInt = ES3.Load<int>("myIntSaved");
            Debug.Log(testInt);
        }
    }
}
