﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasCameraLink : MonoBehaviour
{
    public Canvas canvas;
    
    void Start()
    {
        canvas = GetComponent<Canvas>();
    }
    void Update()
    {
        canvas.worldCamera = Camera.main;
        canvas.sortingLayerName = "UI";
    }
}
