﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MvmtSamuel : MonoBehaviour
{

    public HealthBar healthBar;

    private float _currentHealth;

    private int _maxHealth=50;
    private float _countdownHealth=1f;

    private bool damage;
    private void Start()
    {
        _currentHealth = _maxHealth;
        healthBar.SetMaxHealth(_currentHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector3.right*Time.deltaTime);
        }        
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.up*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.down*Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector3.left*Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _countdownHealth = 1;
            damage = true;
            

        }

        if (damage)
        {
            _countdownHealth -= Time.deltaTime;
            if (_countdownHealth >= 0)
            {
                _currentHealth = Mathf.Lerp(_currentHealth,_currentHealth-10,Time.deltaTime*1f);
                healthBar.SetHealth(_currentHealth);
            }
            else
            {
                damage = false;
            }
        }

    }

}
