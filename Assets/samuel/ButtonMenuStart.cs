﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenuStart : MonoBehaviour
{
    public string SceneToLoad;

    public GameObject panelInstructions;
    //permet de charger la scène voulue
    public void Commencer()
    {
        SceneManager.LoadScene(SceneToLoad);
    }
//affiche le panel des instructions.
    public void InstructionsOn()
    {
        panelInstructions.SetActive(true);
    } 
    //retire le panel des instructions.
    public void InstructionsOff()
    {
        panelInstructions.SetActive(false);
    }

    //Permet de charger la sauvegarde
    public void Load()
    {
        var autoSaveMgr = GameObject.Find("Easy Save 3 Manager").GetComponent<ES3AutoSaveMgr>();
        SceneManager.LoadScene(ES3.Load<string>("SavedScene"));
        autoSaveMgr.Load();
    }
    
//permet de quitter l'application
    public void Quit()
    {
        Application.Quit();
    }
}
