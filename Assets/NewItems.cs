﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class NewItems : MonoBehaviour
{
    public Objects objectGiven;
    public float offsetYItem = 2;

    public GameObject specialPrefab;
    
    public enum Objects
    {
        Lantern,
        WateringCan,
        TimeMachine,
        Doggy,
        Boot
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Nique");
            Inventory inventory = FindObjectOfType<Inventory>();
            
            int intInArray = 0;
            bool needArray = true;
            GameObject prefabWithNoArray = null;
            
            switch (objectGiven)
            {
                case Objects.Lantern:
                    intInArray = 1;
                    break;
                case Objects.WateringCan:
                    intInArray = 2;
                    break;
                case Objects.TimeMachine:
                    intInArray = 4;
                    break;
                case Objects.Doggy:
                    intInArray = 3;
                    break;
                case Objects.Boot:
                    needArray = false;
                    prefabWithNoArray = specialPrefab;
                    FindObjectOfType<PlayerController>().playerHasBoots = true;
                    break;
            }

            if (needArray)
            {
                StartCoroutine(AnimationObtain(inventory.objects[intInArray].prefab));
                inventory.objects[intInArray].active = true;
            }
            else
            {
                StartCoroutine(AnimationObtain(prefabWithNoArray));
            }
        }
    }

    public IEnumerator AnimationObtain(GameObject prefab)
    {
        GameObject tempObj = Instantiate(prefab, transform.position + Vector3.up * offsetYItem, Quaternion.identity);
        tempObj.transform.localScale = new Vector3(2.5f,2.5f,2.5f);
        GetComponent<Animator>().SetTrigger("open");
        yield return new WaitForSeconds(2);
        Destroy(tempObj);
        Destroy(gameObject);
    }
}
